Fidelity Bank of Florida

A community bank www.fbfna.com with the technology features of a large bank AND the personalized service of a local bank. SBA or Commercial Loans, Business and Personal Checking specialists available to assist you.

Address: 1380 N Courtenay Parkway, Merritt Island, FL 32953, USA

Phone: 321-452-0011